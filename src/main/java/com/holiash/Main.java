package com.holiash;

import com.holiash.dwellingmodel.Dwelling;
import com.holiash.dwellingmodel.Flat;
import com.holiash.dwellingmodel.Mansion;
import com.holiash.dwellingview.DwellingView;

import java.util.ArrayList;
import java.util.List;

public class Main {
  public static void main(String[] args) {
    Run run = new Run(initializeDwellings(), new DwellingView());
    run.run();
  }

  static List<Dwelling> initializeDwellings() {
    List<Dwelling> dwellings = new ArrayList<Dwelling>();
    dwellings.add(new Mansion(150, "individual", 5, 15000,
      "Sykhiv", 2, 15));
    dwellings.add(new Flat(100, "central", 3, 75000,
      "Iskra", 5, 9));
    dwellings.add(new Mansion(170, "individual", 7, 150000,
      "Halickiy", 3, 17));
    dwellings.add(new Mansion(100, "individual", 3, 75000,
      "Sykhiv", 1, 12));
    dwellings.add(new Flat(50, "central", 2, 50000,
      "Iskra", 2, 5));
    return dwellings;
  }
}
