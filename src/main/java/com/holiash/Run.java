package com.holiash;

import com.holiash.controller.DwellingController;
import com.holiash.dwellingmodel.Dwelling;
import com.holiash.dwellingview.DwellingView;

import java.util.List;
import java.util.Scanner;

public class Run {
  private static final Scanner scanner = new Scanner(System.in);
  private DwellingView view;
  private List<Dwelling> model;
  private DwellingController controller;

  public Run(List<Dwelling> model, DwellingView view) {
    this.view = view;
    this.model = model;
    this.controller = new DwellingController(model, view);
  }

  public void run() {
    int choice;
    do {
      controller.showMenu();
      choice = scanner.nextInt();
      switch (choice) {
        case 1:
          filterChoice();
          break;
        case 2:
          sortChoice();
          break;
        case 3:
          this.controller.showDwellings();
          break;
        case 9:
          System.out.println("Bye bye");
      }
    } while (choice != 9);
  }

  private void sortChoice() {
    this.controller.showSortMenu();
    int choice = scanner.nextInt();
    switch (choice) {
      case 1:
        this.controller.sortByPrice();
        break;
      case 2:
        this.controller.sortBySquare();
        break;
      case 3:
        this.controller.sortByDistanceToSchool();
        break;
      default:
        System.out.println("Wrong choice");
    }
  }

  private void filterChoice() {
    this.controller.showFilterMenu();
    int choice = scanner.nextInt();
    double from;
    double to;
    switch (choice) {
      case 1:
        System.out.println("Input price from and price to: ");
        from = scanner.nextDouble();
        to = scanner.nextDouble();
        controller.filterByPrice(from, to);
        break;
      case 2:
        System.out.println("Input square from and square to: ");
        from = scanner.nextDouble();
        to = scanner.nextDouble();
        controller.filterBySquare(from, to);
        break;
      case 3:
        System.out.println("Input distance to school from and to: ");
        from = scanner.nextDouble();
        to = scanner.nextDouble();
        controller.filterByDistanceToSchool(from, to);
      default:
        System.out.println("Wrong choice!");
    }
  }
}

