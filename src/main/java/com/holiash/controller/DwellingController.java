package com.holiash.controller;

import com.holiash.dwellingmodel.Dwelling;
import com.holiash.dwellingmodel.heatingtypes.HeatingType;
import com.holiash.dwellingview.DwellingView;

import java.util.ArrayList;
import java.util.List;

public class DwellingController {
  private List<Dwelling> dwellings;
  private DwellingView view;

  public DwellingController(List<Dwelling> dwellings, DwellingView view) {
    this.dwellings = dwellings;
    this.view = view;
  }

  public List<Dwelling> filterBySquare(double squareFrom, double squareTo) {
    if (squareFrom < 0 || squareTo < 0 || squareFrom > squareTo) {
      throw new IllegalArgumentException("Input correct values");
    }
    List<Dwelling> filtered = new ArrayList<Dwelling>();
    for (Dwelling dwelling : this.dwellings) {
      if ((dwelling.getSquare() > squareFrom) && (dwelling.getSquare() < squareTo)) {
        filtered.add(dwelling);
      }
    }
    view.showDwellings(filtered);
    return filtered;
  }

  public List<Dwelling> filterByPrice(double priceFrom, double priceTo) {
    if (priceFrom < 0 || priceTo < 0 || priceFrom > priceTo) {
      throw new IllegalArgumentException("Input correct values");
    }
    List<Dwelling> filtered = new ArrayList<Dwelling>();
    for (Dwelling dwelling : this.dwellings) {
      if ((dwelling.getPrice() > priceFrom) && (dwelling.getPrice() < priceTo)) {
        filtered.add(dwelling);
      }
    }
    view.showDwellings(filtered);
    return filtered;
  }

  public List<Dwelling> filterByHeatingType(String Type) {
    HeatingType heatingType = HeatingType.valueOf(Type.toUpperCase());
    List<Dwelling> filtered = new ArrayList<Dwelling>();
    for (Dwelling dwelling : this.dwellings) {
      if (dwelling.getHeatingType().equals(heatingType)) {
        filtered.add(dwelling);
      }
    }
    view.showDwellings(filtered);
    return filtered;
  }

  public List<Dwelling> filterByDistanceToSchool(double distanceFrom, double distanceTo) {
    if (distanceFrom < 0 || distanceTo < 0 || distanceFrom > distanceTo) {
      throw new IllegalArgumentException("Input correct values");
    }
    List<Dwelling> filtered = new ArrayList<Dwelling>();
    for (Dwelling dwelling : this.dwellings) {
      if ((dwelling.getDistanceToSchool() > distanceFrom) && (dwelling.getDistanceToSchool() < distanceTo)) {
        filtered.add(dwelling);
      }
    }
    view.showDwellings(filtered);
    return dwellings;
  }

  public List<Dwelling> sortByPrice() {
    List<Dwelling> sorted = this.dwellings;
    sorted.sort(((o1, o2) -> (int) (o1.getPrice() - o2.getPrice())));
    view.showDwellings(sorted);
    return sorted;
  }

  public List<Dwelling> sortBySquare() {
    List<Dwelling> sorted = this.dwellings;
    sorted.sort((o1, o2) -> (int) (o1.getSquare() - o2.getSquare()));
    view.showDwellings(sorted);
    return sorted;
  }

  public List<Dwelling> sortByDistanceToSchool() {
    List<Dwelling> sorted = this.dwellings;
    sorted.sort((o1, o2) -> (int) (o1.getDistanceToSchool() - o2.getDistanceToSchool()));
    view.showDwellings(sorted);
    return sorted;
  }

  public void showMenu() {
    this.view.showMenu();
  }

  public void showFilterMenu() {
    this.view.showFilterMenu();
  }

  public void showSortMenu() {
    this.view.showSortMenu();
  }

  public void showDwellings() {
    this.view.showDwellings(this.dwellings);
  }
}
