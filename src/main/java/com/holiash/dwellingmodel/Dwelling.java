package com.holiash.dwellingmodel;

import com.holiash.dwellingmodel.heatingtypes.HeatingType;
import com.holiash.dwellingmodel.location.Location;

public abstract class Dwelling {
  private double square;
  private HeatingType heatingType;
  private int numberOfRooms;
  private Location location;
  private double price;

  public Dwelling(double square, String heatingType, int numberOfRooms, double price, String district) {
    this.square = square;
    this.heatingType = HeatingType.valueOf(heatingType.toUpperCase());
    this.numberOfRooms = numberOfRooms;
    this.price = price;
    this.location = new Location(district);
  }

  public void setHeatingType(String heatingType) {
    this.heatingType = HeatingType.valueOf(heatingType.toUpperCase());
  }

  public void setPrice(double price) {
    if (price < 0) {
      throw new IllegalArgumentException("price must be positive");
    }
    this.price = price;
  }

  public double getSquare() {
    return square;
  }

  public HeatingType getHeatingType() {
    return heatingType;
  }

  public int getNumberOfRooms() {
    return numberOfRooms;
  }

  public double getDistanceToSchool() {
    return this.location.getDistanceToSchool();
  }

  public double getDistanceToKindergarten() {
    return this.location.getDistanceToKindergarten();
  }

  public void setDistanceToSchool(double distance) {
    this.location.setDistanceToSchool(distance);
  }

  public void setDistanceToKindergarten(double distance) {
    this.location.setDistanceToKindergarten(distance);
  }

  public double getPrice() {
    return price;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("square = ").append(this.square).append('\n').append("number of rooms = ").append(this.numberOfRooms)
      .append('\n').append("heating type: ").append(this.heatingType).append('\n')
      .append("district: ").append(this.location.getDistrict()).append('\n')
      .append("price = ").append(this.price);
    return sb.toString();
  }
}
