package com.holiash.dwellingmodel;

public class Flat extends Dwelling {
  private int floor;
  private int numberOfFloors;

  public Flat(double square, String heatingType, int numberOfRooms, double price, String district,
              int floor, int numberOfFloors) {
    super(square, heatingType, numberOfRooms, price, district);
    this.floor = floor;
    this.numberOfFloors = numberOfFloors;
  }

  public int getFloor() {
    return floor;
  }

  public int getNumberOfFloors() {
    return numberOfFloors;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("Type of dwelling: Flat\n").append("floor: ").append(this.floor).append('\n')
      .append("Number of floors: ").append(this.numberOfFloors).append('\n').append(super.toString());
    return sb.toString();
  }
}
