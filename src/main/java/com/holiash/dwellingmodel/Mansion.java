package com.holiash.dwellingmodel;

public class Mansion extends Dwelling {
  private int numberOfFloors;
  private double area;

  public Mansion(double square, String heatingType, int numberOfRooms, double price,
                 String district, int numberOfFloors, double area) {
    super(square, heatingType, numberOfRooms, price, district);
    this.numberOfFloors = numberOfFloors;
    this.area = area;
  }

  public int getNumberOfFloors() {
    return numberOfFloors;
  }

  public double getArea() {
    return area;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("Type of building: Mansion\n").append("area = ").append(this.area).append('\n').append("Number of floors = ")
      .append(this.numberOfFloors).append('\n');
    sb.append(super.toString());
    return sb.toString();
  }
}
