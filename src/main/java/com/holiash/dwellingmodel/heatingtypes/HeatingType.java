package com.holiash.dwellingmodel.heatingtypes;

public enum HeatingType {
  CENTRAL, INDIVIDUAL
}
