package com.holiash.dwellingmodel.location;

public class Location {
  private double distanceToSchool;
  private double distanceToKindergarten;
  private String district;

  public Location(double distanceToSchool, double distanceToKindergarten, String district) {
    this.distanceToSchool = distanceToSchool;
    this.distanceToKindergarten = distanceToKindergarten;
    this.district = district;
  }

  public Location(String district) {
    this.district = district;
  }

  public double getDistanceToSchool() {
    return distanceToSchool;
  }

  public void setDistanceToSchool(double distanceToSchool) {
    if (distanceToSchool < 0) {
      throw new IllegalArgumentException("distance must be positive");
    }
    this.distanceToSchool = distanceToSchool;
  }

  public double getDistanceToKindergarten() {
    return distanceToKindergarten;
  }

  public void setDistanceToKindergarten(double distanceToKindergarten) {
    if (distanceToKindergarten < 0) {
      throw new IllegalArgumentException("distance must be positive");
    }
    this.distanceToKindergarten = distanceToKindergarten;
  }

  public String getDistrict() {
    return district;
  }

  public void setDistrict(String district) {
    this.district = district;
  }
}
