package com.holiash.dwellingview;

import com.holiash.dwellingmodel.Dwelling;

import java.util.List;

public class DwellingView {
  public void showMenu() {
    System.out.println("filter - 1, sort - 2, show - 3, exit - 9");
  }

  public void showFilterMenu() {
    System.out.println("filter by price - 1, filter by square - 2, filter by distance to school - 3");
  }

  public void showSortMenu() {
    System.out.println("sort by price - 1, sort by square - 2, sort by distance to school - 3");
  }

  public void showDwellings(List<Dwelling> dwellings) {
    for (Dwelling dwelling : dwellings) {
      System.out.println(dwelling);
      System.out.println("---------------------------------------");
    }
    System.out.println();
  }
}
